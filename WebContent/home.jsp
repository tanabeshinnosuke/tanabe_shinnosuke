<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored= "false" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix= "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri= "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
	<link rel="stylesheet" href="./css/style.css">

	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="./js/ajax.js"></script>

</head>
<body>

<h1>ホーム画面</h1>


<c:if test="${ not empty loginUser }">

<div class="header">
	<c:if test="${ not empty loginUser }">
	<c:if test="${loginUser.branchId == 1 && loginUser.positionId == 1 }">
		<a href="manage">ユーザー管理画面</a>
	</c:if>
		<a href="newpost">新規投稿画面</a>
		<a href="logout" style="float: right;">ログアウト</a>
	</c:if>
</div>
<hr>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<h3>絞込み</h3>

<form action="./" method="get">
	<span>期間を指定</span>
	<input type="date" value="${Start}" name="start" >～
	<input type="date" value="${End}" name="end" ><br/><br/>
	<label for="category">カテゴリー検索</label>
	<input name="category" value="${Category}" id="category"><br/><br/>

	<input type="submit" value="絞込み実行"><br />
</form>
<br />
<a href="./">検索条件リセット</a>
<hr style="margin_buttom: 30px;">

    <c:forEach items="${posts}" var="post">
    <section class='message-area'>
    <section class='view-area'>
    	<p>投稿</p><br/>
    	<label for="title">件名</label><br />
		<span class="title"><c:out value="${post.title}" /></span><br /><br />

		<label for="main">本文</label><br />
		<c:forEach var="spost" items="${fn:split(post.main, '
			')}">
    		<div class="post-split"><c:out value="${spost}" /></div><br />
		</c:forEach>

		<label for="category">カテゴリー</label><br />
		<div class="category"><c:out value="${post.category}" /></div><br />

		<label for="date">投稿日時</label><br />
		<div class="date"><fmt:formatDate value="${post.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br />

		<label for="post_name">投稿者</label><br />
		<c:forEach items="${userInfo}" var="userInfo">
			<c:if test="${ userInfo.id == post.userId }">
				<div class="post_name"><c:out value="${userInfo.name}" /></div><br />
			</c:if>
		</c:forEach>
		<c:if test="${loginUser.id == post.userId }">
			<form action="postdelete" method="post">
				<input type="hidden" name="id" value="${post.id}">
				<button type="submit" onclick='return confirm("本当に削除しますか？");' >投稿削除</button><br />
			</form>
		</c:if>
    </section>

    <section class='comment-area'>
      <section class='view-area'>
      	<c:forEach items="${comments}" var="comment">
		<c:if test="${post.id == comment.postId }">
		<hr style="margin_buttom: 30px;">
		<p>コメント</p><br/>

			<input type="hidden" name="message_id" value="${comment.id}">

			<label for="text">本文</label><br />

			<c:forEach var="scomment" items="${fn:split(comment.text, '
				')}">
    			<div><c:out value="${scomment}" /></div><br />
			</c:forEach>

			<label for="date"> 投稿日時 </label><br />
			<div class="date"><fmt:formatDate value="${comment.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br />


			<label for="comment_name">投稿者</label><br />
			<c:forEach items="${userInfo}" var="userInfo">
				<c:if test="${ userInfo.id == comment.userId }">
					<div class="comment_name"><c:out value="${userInfo.name}" /></div><br />
				</c:if>
			</c:forEach>
			<c:if test="${loginUser.id == comment.userId }">
				<form action="messagedelete" method="post">
					<input type="hidden" name="message_id" id="message_id" value="${comment.id}">
					<button type="submit" onclick='return confirm("本当に削除しますか？");' >コメント削除</button><br />
				</form>
			</c:if>
			</c:if>
			</c:forEach>
      </section>

      <section class='error-area'>
      	<ul></ul>
      </section>

      <section class='form-area'>
        <form id='register-form'>
        	<c:forEach items="${userInfo}" var="userInfo">
				<c:if test="${ userInfo.id == loginUser.id }">
					<input type="hidden" id="comment_name" value="${userInfo.name}">
				</c:if>
			</c:forEach>
        	<input type='hidden' id='message_id' value="${post.id }">
        	<input type='hidden' id='user_id' value="${loginUser.id }">

        	<%
			java.util.Date now = new java.util.Date();
			java.text.SimpleDateFormat a = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String time = a.format(now);
			%>

			<input type="hidden" id="time" value="<%= time %>">

        	<div>
        	<label id="name-label" for="text">本文</label>
        	<textarea name="text" rows="5" cols="20" id='text' class='comment-text-area'
        	style="margin: 0px 0px 0px 0px; width: 589px; height: 198px; font-size:15px;"></textarea>
        	<span>(500文字以下)</span>
        	</div>
        	<div>
        		<button type='button' id='register'>コメント投稿</button>
        	</div>
        </form>
      </section>

    </section>
    </section>
    </c:forEach>


</c:if>


<div class="copyright">Copyright(c)Shinnosuke Tanabe
</div>
</body>
</html>