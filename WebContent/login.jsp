<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<link rel="stylesheet" href="./css/style.css">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン画面</title>

</head>
<body>
<h1>ログイン画面</h1>

<hr>

<div class="main-contents">

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session" />
</c:if>

<form action="login" method="post" id="Login"><br />
<div class="login_id">
	<label for="login_id" id="login_id_label">ログインID</label>
	<input name="login_id" value="${LoginId}" id="loginId" size=20 style="font-size:20px;"/> <br />

</div>
<div id="password">
	<label for="password" id="password_label">パスワード</label>
	<input name="password" type="password" id="Password" size=20 style="font-size:20px;"/> <br />
</div>
	<br /><input type="submit" value="ログイン"  id="login_bottun"/> <br />

</form>
</div>
<div class="copyright">Copyright(c)Shinnosuke Tanabe</div>
</body>
</html>