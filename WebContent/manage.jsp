<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored= "false" %>
<%@taglib prefix= "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri= "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<link rel="stylesheet" href="./css/style.css">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理画面</title>

</head>
<body>
<h1>ユーザー管理画面</h1>

<div class="header">
	<c:if test="${ not empty loginUser }">
		<a href="signup">ユーザー新規登録画面</a>
		<a href="./">ホーム画面</a>
		<a href="logout" style="float: right;">ログアウト</a>
	</c:if>
</div>
<hr>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<div id="userInfo">
	<c:forEach items="${userInfo}" var="userInfo">
	<ul>

		<li  id="login-id">ログインID</li>
		<li class="login_id"><c:out value="${userInfo.loginId}" /></li>

		<li id="name">名称</li>
		<li class="name"><c:out value="${userInfo.name}" /></li>

		<li id="branch">支店</li>
				<c:forEach items="${branch}" var="branch">
			<c:if test="${ branch.id == userInfo.branchId }">
				<li class="branch_name"><c:out value="${branch.name}" /></li>
			</c:if>
		</c:forEach>

		<li id="position">部署・役職</li>
		<c:forEach items="${position}" var="position">
			<c:if test="${ position.id == userInfo.positionId }">
				<li class="position_name"><c:out value="${position.name}" /></li>
			</c:if>
		</c:forEach>
		</ul>


	<form action="manage" method="post" style="display: inline" id="account">
			<input type="hidden" name="id" value="${userInfo.id}">
			<input type="hidden" name="is_deleted" value="${userInfo.isDeleted}">
			<c:if test="${userInfo.isDeleted == 0}">
			<label for="is_deleted" id="account-stop">アカウント停止中</label>
			<c:if test="${loginUser.id != userInfo.id}">
				<button type="submit" onclick='return confirm("本当に復活しますか？");' >復活</button>
			</c:if>
			</c:if>
			<c:if test="${userInfo.isDeleted == 1}">
			<label for="is_deleted" id="account-move">アカウント稼動中</label>
			<c:if test="${loginUser.id != userInfo.id}">
				<button type="submit" onclick='return confirm("本当に停止しますか？");'>停止</button>
			</c:if>
			</c:if>
	</form>

		<a href="useredit?id=${userInfo.id}">ユーザー編集画面</a><br /><br />


		<hr>
	</c:forEach>
</div>

<div class="copyright">Copyright(c)Shinnosuke Tanabe
</div>
</body>
</html>