<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<link rel="stylesheet" href="./css/style.css">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿画面</title>
</head>
<body>

<h1>新規投稿画面</h1>
<a href="./">ホーム画面</a>
<a href="logout" style="float: right;">ログアウト</a>
<hr>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session" />
</c:if>

<div class="new-posts">

<form action="newpost" method="post" id="new-posts">
	<label for="title">件名</label>
	<input name="title" value="${Posts.title}" id="title" size=20 style="font-size:18px;">
	<span>(30文字以下)</span><br /><br />

	<label for="main">本文</label>
	<textarea name="main" class="main-box" style="margin: 0px; height: 186px; width: 600px; font-size: 18px;" ><c:if test="${ not empty Posts }"><c:out value="${Posts.main}"></c:out>
	</c:if></textarea>
	<span style="float: right;">(1,000文字以下)</span><br /><br />

	<label for="category">カテゴリー</label>
	<input name="category" value="${Posts.category}" id="category" size=20 style="font-size:18px;">
	<span>(10文字以下)</span><br />
	<div class="PostBottun">
		<input type="submit" value="投稿" class="postBottun"><br /><br />
	</div>


</form>

</div>


<div class="copyright">Copyright(c)Shinnosuke Tanabe</div>
</body>
</html>