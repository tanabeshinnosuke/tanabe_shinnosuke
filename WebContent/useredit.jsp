<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored = "false" %>
<%@taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<link rel="stylesheet" href="./css/style.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー編集画面</title>
</head>
<body>
<h1>ユーザー編集画面</h1>
<a href="manage" id="return-manage">ユーザー管理画面</a>
<a href="logout" style="float: right;">ログアウト</a>
<hr>
<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>


<div class="edit">
<form action="useredit" method="post" id="edit"><br />

	<input type="hidden" name="id" value="${User.id}">


	<label for="login_id">ログインID</label>
	<input name="login_id" value="${User.loginId}" id="login_id" size=20 style="font-size:20px;"/><br /><br />


	<label for="password">パスワード</label>
	<input name="password" type="password"  size=20 style="font-size:20px;"/><br /><br />


	<label for="passwordConfirm">パスワード確認用</label>
	<input name="passwordConfirm" type="password" id="passwordConfirm" size=20 style="font-size:20px;"/><br/><br />


	<label for="name">名称</label>
	<input name="name" value="${User.name}" id="name" size=20 style="font-size:20px;"/><br /><br />


	<label for="branch_id">支店</label>
	<c:if test="${loginUser.id != User.id}">
	<select name="branch_id">
	<option value="0">選択してください</option>
	<c:forEach items="${Branch}" var="Branch">

	<c:if test="${Branch.id == User.branchId }">
		<option value="${Branch.id}" selected = "selected"><c:out value="${Branch.name}"></c:out></option>
	</c:if>

	<c:if test="${Branch.id != User.branchId }">
		<option value="${Branch.id}"><c:out value="${Branch.name}"></c:out></option>
	</c:if>

	</c:forEach>
	</select>

	</c:if>

	<c:if test="${loginUser.id == User.id}">
	<c:forEach items="${Branch}" var="Branch">

	<c:if test="${Branch.id == User.branchId }">
		<c:out value="${Branch.name}"></c:out>
		<input type="hidden" name="branch_id" value="${User.branchId}">
	</c:if>

	</c:forEach>
	</c:if><br /><br />


	<label for="position_id">部署・役職</label>
	<c:if test="${loginUser.id != User.id}">
	<select name="position_id">
	<option value="0">選択してください</option>
	<c:forEach items="${Position}" var="Position">

	<c:if test="${Position.id == User.positionId }">
		<option value="${Position.id}" selected = "selected"><c:out value="${Position.name}"></c:out></option>
	</c:if>

	<c:if test="${Position.id != User.positionId }">
		<option value="${Position.id}"><c:out value="${Position.name}"></c:out></option>
	</c:if>

	</c:forEach>
	</select>

	</c:if>

	<c:if test="${loginUser.id == User.id}">
	<c:forEach items="${Position}" var="Position">

	<c:if test="${Position.id == User.positionId }">
		<c:out value="${Position.name}"></c:out>
		<input type="hidden" name="position_id" value="${User.positionId}">
	</c:if>

	</c:forEach>
	</c:if><br /><br />


	<input type="hidden" name="is_deleted" value="${User.isDeleted}">

	<input type="submit" value="更新" id="update-bottun" /><br />

</form>

</div>
<div class="copyright">Copyright(c)Shinnosuke Tanabe</div>
</body>
</html>