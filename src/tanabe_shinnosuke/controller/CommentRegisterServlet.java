package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tanabe_shinnosuke.beans.Comment;
import tanabe_shinnosuke.beans.Message;
import tanabe_shinnosuke.service.MessageService;

@WebServlet(urlPatterns = { "/comment/register" })
public class CommentRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Comment comment = createCommentData(request);
		List<String> errorMessages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

		if(isValid(comment, errorMessages)) {
			// バリデーション成功
			System.out.println("message_id = " + comment.getMessage_id());
			System.out.println("user_id = " + comment.getUser_id());
			System.out.println("text = " + comment.getText());
			//Ajax
			try {
				int autoIncKey = new MessageService().register(comment);
				comment.setId(autoIncKey);

				//keyでコメント取得
				Message comments = new MessageService().getComment(autoIncKey);
				String o = json(comments);

				// 成功情報を返す
				isSuccess = true;
				responseJson = String.format("{\"is_success\" : \"%s\", \"o\" : %s}", isSuccess, o);

			} catch (SQLException e) {

				e.printStackTrace();
			}


		} else {
			// バリデーションエラー
			// ListをJsonの形にして返す
			errors = new ObjectMapper().writeValueAsString(errorMessages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
		}

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
	}

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private Comment createCommentData(HttpServletRequest request) {
		String data = request.getParameter("comment");

		Comment comment = new Comment();
		try {

			comment = new ObjectMapper().readValue(data, Comment.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return comment;
	}

	/*
	 * JavaオブジェクトをJson文字列に変換するメソッド
	 */
	 public String json(Message comme) throws JsonProcessingException {

	        ObjectMapper mapper = new ObjectMapper();
	        String json = mapper.writeValueAsString(comme);

	        return json;
	    }

	/*
	 * 入力データの検証を行うメソッド
	 */
	private boolean isValid(Comment comment, List<String> errorMessages) {
		if(StringUtils.isBlank(comment.getText())) {
			errorMessages.add("コメントを入力してください。");
		} else if(comment.getText().length() > 500) {
			errorMessages.add("コメントは500文字以下で入力してください。");
		}

		if(errorMessages.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

}
