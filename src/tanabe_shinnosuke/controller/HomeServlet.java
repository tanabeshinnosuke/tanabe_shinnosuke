package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import tanabe_shinnosuke.beans.Message;
import tanabe_shinnosuke.beans.Post;
import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.service.MessageService;
import tanabe_shinnosuke.service.PostService;
import tanabe_shinnosuke.service.UserService;


@WebServlet(urlPatterns = { "/index.jsp"})

public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {

		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String begin = "2017/11/01";
		String time = sdf.format(now);


		//絞込み
		String start = request.getParameter("start");
		String end = request.getParameter("end");

		String category = request.getParameter("category");

		if(StringUtils.isBlank(start) == true && StringUtils.isBlank(end) == true) { //指定なし
			List<Post> posts = new PostService().getPost(begin, time, category);
			request.setAttribute("posts", posts);
		}

		if(StringUtils.isBlank(start) == true && StringUtils.isBlank(end) == false) { //終わりのみ
			List<Post> posts = new PostService().getPost(begin, end, category);
			request.setAttribute("posts", posts);
		}

		if(StringUtils.isBlank(start) == false && StringUtils.isBlank(end) == true) { //開始のみ
			List<Post> posts = new PostService().getPost(start, time, category);
			request.setAttribute("posts", posts);
		}

		if(StringUtils.isBlank(start) == false && StringUtils.isBlank(end) == false) { //両方あり
			List<Post> posts = new PostService().getPost(start, end, category);
			request.setAttribute("posts", posts);
		}

		List<User> userInfo = new UserService().getUserInfo();

		List<Message> messages = new MessageService().getMessage();

		request.setAttribute("userInfo", userInfo);
		request.setAttribute("comments", messages);
		request.setAttribute("Start", start);
		request.setAttribute("End", end);
		request.setAttribute("Category", category);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}