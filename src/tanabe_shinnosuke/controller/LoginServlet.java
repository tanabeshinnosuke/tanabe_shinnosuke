package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.service.LoginService;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
	private static final long serivalVersionUID =1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(login_id, password);
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		if(user != null && user.getIsDeleted() == 1) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		} else if(user != null && user.getIsDeleted() == 0) {

			messages.add("アカウント停止中です");
			request.setAttribute("LoginId", request.getParameter("login_id")); //値保持

			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("login.jsp").forward(request, response);

		} else {

			messages.add("ログインに失敗しました");

			request.setAttribute("LoginId", request.getParameter("login_id")); //値保持

			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}

	}
}