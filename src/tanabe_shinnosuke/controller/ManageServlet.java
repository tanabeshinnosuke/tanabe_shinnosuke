package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanabe_shinnosuke.beans.Branch;
import tanabe_shinnosuke.beans.Position;
import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.service.BranchService;
import tanabe_shinnosuke.service.PositionService;
import tanabe_shinnosuke.service.UserService;




@WebServlet(urlPatterns = { "/manage" })
public class ManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<User> userInfo = new UserService().getUserInfo();
		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		request.setAttribute("userInfo", userInfo);
		request.setAttribute("branch", branch);
		request.setAttribute("position", position);

		request.getRequestDispatcher("/manage.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User user = getUser(request);

		new UserService().stop(user);
		request.setAttribute("User", user);
		response.sendRedirect("manage");
	}

	private User getUser(HttpServletRequest request)
			throws IOException, ServletException {

		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("id")));

		if(Integer.parseInt(request.getParameter("is_deleted")) == 1) {
			user.setIsDeleted(Integer.parseInt("0"));
		}

		if(Integer.parseInt(request.getParameter("is_deleted")) == 0) {
			user.setIsDeleted(Integer.parseInt("1"));
		}

		return user;
	}

}