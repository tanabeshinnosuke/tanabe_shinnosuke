package tanabe_shinnosuke.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanabe_shinnosuke.beans.Message;
import tanabe_shinnosuke.service.MessageService;

@WebServlet(urlPatterns = { "/messagedelete" })
public class MessageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Message message = getMessage(request);

		new MessageService().delete(message);
		request.setAttribute("comments", message);
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request)
			throws IOException, ServletException {

		Message message = new Message();

		message.setId(Integer.parseInt(request.getParameter("message_id")));

		return message;
	}

}