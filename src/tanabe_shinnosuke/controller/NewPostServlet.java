package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tanabe_shinnosuke.beans.Post;
import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.service.PostService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {
		request.getRequestDispatcher("newpost.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setTitle(request.getParameter("title"));
			post.setMain(request.getParameter("main"));
			post.setCategory(request.getParameter("category"));
			post.setUserId(user.getId());

			new PostService().register(post);

			response.sendRedirect("./");
		} else {

			Post post = new Post();
			post.setTitle(request.getParameter("title"));
			post.setMain(request.getParameter("main"));
			post.setCategory(request.getParameter("category"));

			request.setAttribute("Posts", post);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) { //バリデーション

		String title = request.getParameter("title");
		String main = request.getParameter("main");
		String category = request.getParameter("category");

		if(StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}
		if(StringUtils.isBlank(title) == false && 30 < title.length()) {
			messages.add("件名は30文字以下で入力してください");
		}

		if(StringUtils.isBlank(main) == true) {
			messages.add("本文を入力してください");
		}

		if(StringUtils.isBlank(main) == false && 1000 < main.length()) {
			messages.add("本文は1,000字以内で入力してください");
		}

		if(StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}

		if(StringUtils.isBlank(category) == false && 10 < category.length()) {
			messages.add("カテゴリーは10字以内で入力してください");
		}

		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}