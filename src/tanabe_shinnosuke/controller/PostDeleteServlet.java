package tanabe_shinnosuke.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanabe_shinnosuke.beans.Post;
import tanabe_shinnosuke.service.PostService;

@WebServlet(urlPatterns = { "/postdelete" })
public class PostDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Post post = getPost(request);

		new PostService().delete(post);
		request.setAttribute("posts", post);
		response.sendRedirect("./");
	}

	private Post getPost(HttpServletRequest request)
			throws IOException, ServletException {

		Post post = new Post();

		post.setId(Integer.parseInt(request.getParameter("id")));

		return post;
	}

}
