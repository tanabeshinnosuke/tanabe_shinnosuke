package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import tanabe_shinnosuke.beans.Branch;
import tanabe_shinnosuke.beans.Position;
import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.service.BranchService;
import tanabe_shinnosuke.service.PositionService;
import tanabe_shinnosuke.service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {

		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		request.setAttribute("Branch", branch);
		request.setAttribute("Position", position);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
		ServletException {

		List<String> message = new ArrayList<String>();

		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		if(isValid(request, message) == true) {
			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
			user.setPositionId(Integer.parseInt(request.getParameter("position_id")));
			user.setIsDeleted(Integer.parseInt("1"));

			new UserService().register(user);

			response.sendRedirect("manage");
		} else {

			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
			user.setPositionId(Integer.parseInt(request.getParameter("position_id")));

			request.setAttribute("SignUpUser", user);

			request.setAttribute("Branch", branch);
			request.setAttribute("Position", position);

			request.setAttribute("errorMessages", message);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}

		private boolean isValid(HttpServletRequest request, List<String> messages) {
			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");
			String passwordConfirm = request.getParameter("passwordConfirm");
			String name = request.getParameter("name");
			String branchId = request.getParameter("branch_id");
			String positionId = request.getParameter("position_id");

			User LoginId = new UserService().LoginId(loginId);


			if(LoginId != null) {
				messages.add("入力されたログインIDはすでに使用されています");
			}

			if(StringUtils.isBlank(loginId) == true) {
				messages.add("ログインIDを入力してください");
			}

			if(StringUtils.isBlank(loginId) == false && (20 < loginId.length() || 6 > loginId.length())) {
				messages.add("ログインIDは6文字以上、20文字以内で入力してください");
			}

			if(StringUtils.isBlank(loginId) == false && loginId.matches("\\W*")) {
				messages.add("ログインIDは半角英数字で入力してください");
			}

			if(StringUtils.isBlank(password) == true) {
				messages.add("パスワードを入力してください");
			}

			if(StringUtils.isBlank(password) == false && (20 < password.length() || 6 > password.length())) {
				messages.add("パスワードは6文字以上、20文字以内で入力してください");
			}

			if(password.equals(passwordConfirm)) {

			} else {
				messages.add("入力されたパスワードと確認用パスワードが一致しません");
			}

			if(StringUtils.isBlank(name) == true) {
				messages.add("名称を入力してください");
			}

			if(StringUtils.isBlank(name) == false && 10 < name.length()) {
				messages.add("名称は10文字以内で入力してください");
			}

			if(branchId.matches("0")) {
				messages.add("支店を選択してください");
			}

			if(positionId.matches("0")) {
				messages.add("部署・役職を選択してください");
			}

			if((positionId.equals("1") || positionId.equals("2")) && branchId.equals("1") == false) {
				messages.add("支店と部署・役職が不正な組み合わせです");
			}

			if(branchId.equals("1") && (positionId.equals("3") || positionId.equals("4")) == true) {
				messages.add("支店と部署・役職が不正な組み合わせです");
			}

			if(messages.size() == 0) {
				return true;
			} else {
				return false;
			}
		}

	}
