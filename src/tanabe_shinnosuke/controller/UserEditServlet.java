package tanabe_shinnosuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tanabe_shinnosuke.beans.Branch;
import tanabe_shinnosuke.beans.Position;
import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.exception.NoRowsUpdatedRuntimeException;
import tanabe_shinnosuke.service.BranchService;
import tanabe_shinnosuke.service.PositionService;
import tanabe_shinnosuke.service.UserService;

@WebServlet(urlPatterns = { "/useredit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		UserService editService = new UserService();

		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		request.setAttribute("Branch", branch);
		request.setAttribute("Position", position);

		if(StringUtils.isBlank(id) == true) {
			messages.add("ユーザーが指定されていません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("manage");
			return;
		}

		if(StringUtils.isBlank(id) == false && id.matches("\\D*")) {

			messages.add("不正な値が入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("manage");
			return;

		} else {
			int ID = Integer.parseInt(id);
			User user = editService.edited(ID);
			if(user != null) {
				request.setAttribute("User", user);

				request.getRequestDispatcher("useredit.jsp").forward(request, response);
			} else {
				messages.add("不正な値が入力されました");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("manage");
				return;
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		request.setAttribute("Branch", branch);
		request.setAttribute("Position", position);

		User user = getUser(request);

		if (isValid(request, messages) == true) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("useredit");
			}
				session.setAttribute("User", user);
				response.sendRedirect("manage"); //正常終了
			} else {
				request.setAttribute("User", user);
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("useredit.jsp").forward(request, response);
		}
	}

	private User getUser(HttpServletRequest request)
			throws IOException, ServletException {

		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setLoginId(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		user.setPositionId(Integer.parseInt(request.getParameter("position_id")));
		user.setIsDeleted(Integer.parseInt(request.getParameter("is_deleted")));

		return user;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		int editedId = Integer.parseInt(request.getParameter("id"));
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branch_id");
		String positionId = request.getParameter("position_id");

		User LoginId = new UserService().LoginId(loginId);

		if(LoginId != null) {
			int id = LoginId.getId();

			if(editedId != id) {
				messages.add("入力されたログインIDはすでに使用されています");
			}
		}

		if(StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}

		if(StringUtils.isBlank(loginId) == false && (20 < loginId.length() || 6 > loginId.length())) {
			messages.add("ログインIDは6文字以上、20文字以内で入力してください");
		}

		if(StringUtils.isBlank(loginId) == false && loginId.matches("\\W*")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}

		if(StringUtils.isBlank(password)) {

		} else {
			if(20 < password.length() || 6 > password.length()) {
				messages.add("パスワードは6文字以上、20文字以内で入力してください");
			}
		}

		if(password.equals(passwordConfirm)) {

		} else {
			messages.add("入力されたパスワードと確認用パスワードが一致しません");
		}

		if(StringUtils.isBlank(name) == true) {
			messages.add("名称を入力してください");
		}

		if(StringUtils.isBlank(name) == false && 10 < name.length()) {
			messages.add("名称は10文字以内で入力してください");
		}

		if(branchId.equals("0")) {
			messages.add("支店を選択してください");
		}

		if(positionId.equals("0")) {
			messages.add("部署・役職を選択してください");
		}

		if((positionId.equals("1") || positionId.equals("2")) && branchId.equals("1") == false) {
			messages.add("支店と部署・役職が不正な組み合わせです");
		}

		if(branchId.equals("1") && (positionId.equals("3") || positionId.equals("4")) == true) {
			messages.add("支店と部署・役職が不正な組み合わせです");
		}

		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}