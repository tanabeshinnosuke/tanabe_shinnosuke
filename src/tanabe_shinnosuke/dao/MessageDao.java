package tanabe_shinnosuke.dao;

import static tanabe_shinnosuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import tanabe_shinnosuke.beans.Comment;
import tanabe_shinnosuke.beans.Message;
import tanabe_shinnosuke.exception.SQLRuntimeException;

public class MessageDao {
	// コメント投稿
	public void insert(Connection connection, Message message ) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("text");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(") VALUES (");
			sql.append("?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(", ?");// user_id
			sql.append(", ?"); // post_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getText());
			ps.setInt(2, message.getUserId());
			ps.setInt(3, message.getPostId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//コメント表示
	public List<Message> getUserMessage(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM messages ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Message> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//コメント表示
	private List<Message> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<Message> ret = new ArrayList<Message>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String text = rs.getString("text");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				int userId = rs.getInt("user_id");
				int postId = rs.getInt("post_id");

				Message userMessage = new Message();
				userMessage.setId(id);
				userMessage.setText(text);
				userMessage.setInsertDate(insertDate);
				userMessage.setUserId(userId);
				userMessage.setPostId(postId);

				ret.add(userMessage);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//Ajax
	/*public void insert(Connection connection, Comment comment ) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("text");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(") VALUES (");
			sql.append("?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(", ?");// user_id
			sql.append(", ?"); // post_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUser_id());
			ps.setInt(3, comment.getMessage_id());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}*/

	public int insert(Connection connection, Comment comment ) throws SQLException {
		 PreparedStatement ps = null;
		 ResultSet rs                = null;
		 int autoIncKey              = -1;
		 try {
			 StringBuilder sql = new StringBuilder();
			 sql.append("INSERT INTO messages ( ");
			 sql.append("text");
			 sql.append(", insert_date");
			 sql.append(", update_date");
			 sql.append(", user_id");
			 sql.append(", post_id");
			 sql.append(") VALUES (");
			 sql.append("?"); // text
			 sql.append(", CURRENT_TIMESTAMP"); // insert_date
			 sql.append(", CURRENT_TIMESTAMP"); // update_date
			 sql.append(", ?");// user_id
			 sql.append(", ?"); // post_id
			 sql.append(")");

		  //引数にjava.sql.Statement.RETURN_GENERATED_KEYSを指定
		  //DBコネクションは既に取得ずみとする
			ps = connection.prepareStatement(sql.toString(),
		           java.sql.Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUser_id());
			ps.setInt(3, comment.getMessage_id());

		  //追加
			ps.executeUpdate();
		  //auto-incrementの値取得
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
		 } catch (SQLException e) {
			 throw e;
		 } finally {
		  //ResultSetクローズ
			 if (rs != null) {
				 try {
					 rs.close();
				 } catch (SQLException e) {
					 e.printStackTrace();
				 }
			 }
		  //statementクローズ
			 if (ps != null) {
				 try {
					 ps.close();
				 } catch (SQLException e) {
					 e.printStackTrace();
				 }
			 }
		 }

		 return autoIncKey;
	}

	//Ajax コメント表示keyで取得
	public Message getComment(Connection connection, int key) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM messages WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, key);

			ResultSet rs = ps.executeQuery();
			List<Message> ret = toUserMessageList(rs);
			if (ret.isEmpty() == true) {
				return null;
			} else if (2 <= ret.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return ret.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}