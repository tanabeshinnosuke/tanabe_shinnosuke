package tanabe_shinnosuke.dao;

import static tanabe_shinnosuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tanabe_shinnosuke.beans.Post;
import tanabe_shinnosuke.exception.SQLRuntimeException;

public class PostDao {

	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("title");
			sql.append(", main");
			sql.append(", category");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(", user_id");
			sql.append(") VALUES (");
			sql.append("?"); // title
			sql.append(", ?"); // main
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(", ?"); // user_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, post.getTitle());
			ps.setString(2, post.getMain());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getUserId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//投稿削除
	public void delete(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM posts WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, post.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//投稿表示・絞込み
	public List<Post> getPost(Connection connection, int num, String start, String end,
			String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			if(StringUtils.isBlank(category)) {
				sql.append("SELECT * FROM posts WHERE insert_date BETWEEN  ?  AND  ? "
						+ "ORDER BY insert_date DESC");

			} else {
				sql.append("SELECT * FROM posts WHERE insert_date BETWEEN  ?  AND  ? "
						+ "&& category LIKE  ? ORDER BY insert_date DESC");
			}

			ps = connection.prepareStatement(sql.toString());
			if(StringUtils.isBlank(category)) {
				ps.setString(1, start);
				ps.setString(2, end + "/23:59:59");
			} else {
				ps.setString(1, start);
				ps.setString(2, end + "/23:59:59");
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//投稿表示
	private List<Post> toPostList(ResultSet rs)
			throws SQLException {

		List<Post> ret = new ArrayList<Post>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String main = rs.getString("main");
				String category = rs.getString("category");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				int userId = rs.getInt("user_id");

				Post posts = new Post();
				posts.setId(id);
				posts.setTitle(title);
				posts.setMain(main);
				posts.setCategory(category);
				posts.setInsertDate(insertDate);
				posts.setUserId(userId);

				ret.add(posts);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}