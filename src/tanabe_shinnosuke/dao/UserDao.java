package tanabe_shinnosuke.dao;


import static tanabe_shinnosuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.exception.NoRowsUpdatedRuntimeException;
import tanabe_shinnosuke.exception.SQLRuntimeException;

//ログイン機能
public class UserDao {
	public User getUser(Connection connection, String login_id, String password) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id =  ? AND password = ?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				int is_deleted = rs.getInt("is_deleted");

				User user = new User();
				user.setId(id);
				user.setLoginId(login_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branch_id);
				user.setPositionId(position_id);
				user.setIsDeleted(is_deleted);

				ret.add(user);
			}
				return ret;
		} finally {
			close(rs);
		}
	}

	//ユーザー登録
	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO users (");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?"); //login_id
			sql.append(", ?"); //password
			sql.append(", ?"); //name
			sql.append(", ?"); //branch_id
			sql.append(", ?"); //position_id
			sql.append(", ?"); //is_deleted
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());
			ps.setInt(6, user.getIsDeleted());

			ps.executeUpdate();

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//新規登録 ログインIDバリデーション
	public User getLoginId(Connection connection, String login_id) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id =  ?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();
			List<User> editedList = toUserList(rs);
			if (editedList.isEmpty() == true) {
				return null;
			} else if (2 <= editedList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return editedList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	//ユーザー管理
	public List<User> getUserInfo(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users ORDER BY branch_id , position_id";
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー編集 対象者情報
	public User getUser(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id =  ?";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> editedList = toUserList(rs);
			if (editedList.isEmpty() == true) {
				return null;
			} else if (2 <= editedList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return editedList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//ユーザー編集
	public void update(Connection connection, User user) {
		String password = user.getPassword();
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			if(StringUtils.isBlank(password)) {

			} else {
				sql.append(", password = ?");
			}
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			if(StringUtils.isBlank(password)) {
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getIsDeleted());
				ps.setInt(6, user.getId());
			} else {
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getPositionId());
				ps.setInt(6, user.getIsDeleted());
				ps.setInt(7, user.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//アカウント停止復活
	public void stop(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsDeleted());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}