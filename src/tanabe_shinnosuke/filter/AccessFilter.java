package tanabe_shinnosuke.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanabe_shinnosuke.beans.User;

@WebFilter(urlPatterns = {"/manage", "/signup","/useredit"})
public class AccessFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ( (HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();

		if(user != null) {
			System.out.println("アクセスフィルター");

			int branchId = user.getBranchId();
			int positionId = user.getPositionId();

		if(branchId == 1 && positionId == 1) {

			} else {

				messages.add("アクセス権限がないページのため、ホーム画面に戻りました");

				session.setAttribute("errorMessages", messages);
				((HttpServletResponse) response).sendRedirect("./");
				return;
			}
		}

		chain.doFilter(request, response); // サーブレットを実行
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}
}