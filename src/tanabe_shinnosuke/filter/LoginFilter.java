package tanabe_shinnosuke.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanabe_shinnosuke.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {


	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ( (HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();

		String servletpath = ((HttpServletRequest) request).getServletPath();

		System.out.println(servletpath);
		if((servletpath.equals("/login") == false && !servletpath.matches("/css/.*"))) {

			if(user == null) {
				System.out.println("ログインフィルター");
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse) response).sendRedirect("login");
				return;
			}
		}

		chain.doFilter(request, response); // サーブレットを実行
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}
}