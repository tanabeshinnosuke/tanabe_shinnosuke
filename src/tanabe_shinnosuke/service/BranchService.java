package tanabe_shinnosuke.service;

import static tanabe_shinnosuke.utils.CloseableUtil.*;
import static tanabe_shinnosuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanabe_shinnosuke.beans.Branch;
import tanabe_shinnosuke.dao.BranchDao;

public class BranchService {
	public List<Branch> getBranch() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> branch = branchDao.getBranch(connection);

			commit(connection);

			return branch;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
