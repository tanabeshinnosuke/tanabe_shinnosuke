package tanabe_shinnosuke.service;

import static tanabe_shinnosuke.utils.CloseableUtil.*;
import static tanabe_shinnosuke.utils.DBUtil.*;

import java.sql.Connection;

import tanabe_shinnosuke.beans.User;
import tanabe_shinnosuke.dao.UserDao;
import tanabe_shinnosuke.utils.CipherUtil;

public class LoginService {
	public User login(String login_id, String password) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, login_id, encPassword);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
