package tanabe_shinnosuke.service;

import static tanabe_shinnosuke.utils.CloseableUtil.*;
import static tanabe_shinnosuke.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import tanabe_shinnosuke.beans.Comment;
import tanabe_shinnosuke.beans.Message;
import tanabe_shinnosuke.dao.MessageDao;

public class MessageService {
	//コメント投稿
	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//コメント表示
	private static final int LIMIT_NUM = 1000;

	public List<Message> getMessage() {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			List<Message> ret = messageDao.getUserMessage(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//コメント削除
	public void delete(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//Ajax コメント登録
	public int register(Comment comment) throws SQLException {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			int autoIncKey = messageDao.insert(connection, comment);

			commit(connection);
			return autoIncKey;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//Ajax コメント表示

	public Message getComment(int key) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			Message ret = messageDao.getComment(connection, key);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}