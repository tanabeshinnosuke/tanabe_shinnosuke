package tanabe_shinnosuke.service;

import static tanabe_shinnosuke.utils.CloseableUtil.*;
import static tanabe_shinnosuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanabe_shinnosuke.beans.Position;
import tanabe_shinnosuke.dao.PositionDao;

public class PositionService {
	public List<Position> getPosition() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Position> position = positionDao.getPosition(connection);

			commit(connection);

			return position;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
