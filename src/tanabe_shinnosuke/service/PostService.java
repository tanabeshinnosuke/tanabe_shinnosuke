package tanabe_shinnosuke.service;

import static tanabe_shinnosuke.utils.CloseableUtil.*;
import static tanabe_shinnosuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanabe_shinnosuke.beans.Post;
import tanabe_shinnosuke.dao.PostDao;


public class PostService {

	//投稿
	public void register(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//投稿表示
	private static final int LIMIT_NUM = 1000;

	public List<Post> getPost(String start, String end, String category) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			List<Post> ret = postDao.getPost(connection, LIMIT_NUM, start, end, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//投稿削除
	public void delete(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.delete(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}